##################################################################
### Terrain Categories
###
### Terrain types: plains, mountains, hills, desert, artic, forest, jungle, marsh, pti
### Types are used by the game to apply certain bonuses/maluses on movement/combat etc.
###
### Sound types: plains, forest, desert, sea, jungle, mountains

categories =  {
	pti = {
		type = pti
	}

	ocean = {
		color = { 255 255 255 }

		sound_type = sea
		is_water = yes

		movement_cost = 1.0
	}

	inland_ocean = {
		color = { 0 0 200 }

		sound_type = sea
		is_water = yes
		inland_sea = yes

		movement_cost = 1.0
	}	

	glacier = {
		color = { 235 235 235 }

		sound_type = desert

		defence = 1
		movement_cost = 1.25
		supply_limit = 2		
		cs_only_local_development_cost = 0.5
		nation_designer_cost_multiplier = 0.75		
		
		terrain_override = {		
		
		}
	}

	farmlands = {
		color = { 179 255 64 }

		type = plains
		sound_type = plains

		movement_cost = 1.10
		local_development_cost = -0.05
		supply_limit = 10
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1.05
		
		terrain_override = {
			85 88 84 81 97 101 80 79 95 108 114 73 38 67 74 70 78 82 83 119 87 125 128 126 133 132 131 130 136 137 135 152 163 166 162 164 153 156 10 15 13 33 34 49 43 46 26 27 23 24 86 20 113 112
			143 141 103 147 165 149 12 151 63 66 207 347 177 176 175 181 171 158 44 159 142
			216 251 249 915 233 234 908 253 236 220 337 339 221 335 256 914 262 8 168 3 6
			#Dameria
			365 39 50 331 285 284 329 328 281 910 330 275 327 297 280 916 274 333 269 267 917 109 587 286 293 298
			428 432 433 #Corvuria
			
			#Lencenor
			32 59 124
			
			#Dragon Coast
			172 144 145
			
			#Borders
			325 904 921 417 412 415 919 319 315 300 294 332 923 579 907
			
			#Esmaria
			311 310 911 265 266 901 909 902 279
			
			#Alenic Expanse
			226 218 340 243 219 245
			
			#Alenic Reach
			695 699 724 723 721 709 718
			
			#Escann
			779 777 774 781 756 757 765 792 835 834 837 839 832 833 831 829 830 821 825 827 826 856 876 885 818 817 816 810 819 800 798 797
			
			#Bulwar
			574 567 584 560 586 596 598 600 599 601 602 604
			607 612 625 628 631 634 635 639 640 641 642 643 608 613 615 616
			622 621 496 516 521 538 543
			
			#Kheterata
			468 469 471 472 473 474 475
		}		
	}
	
	forest = {
		color = { 31 116 16 }

		type = forest
		sound_type = forest
		
		movement_cost = 1.25
		defence = 1
		supply_limit = 4
		cs_only_local_development_cost = 0.2
		nation_designer_cost_multiplier = 0.9
		
		terrain_override = { 
			110 57 5 228 178 140 137 292 717
		}                                      
	}
	
	hills = {
		color = { 113 176 151 }

		type = hills
		sound_type = mountain

		movement_cost = 1.40
		defence = 1
		local_defensiveness = 0.1
		cs_only_local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85
		supply_limit = 5
		
		terrain_override = {
			100 93 90 72 71 161 160 18 28 17 167 148 150 211 212 336 217 346 254 367 362 2 170 174 173 48 366 838 306 305 354
			37 276 277 261 304 303 91
			424  #Corvuria
			
			#Bulwar
			675 678 677 523 514 515 528 530 531
			
			#Aelantir 
			1094 1074 2091
		}
	}

	
	woods = {
		color = { 41 155 22 }

		type = forest
		sound_type = forest

		movement_cost = 1.10
		defence = 1
		cs_only_local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.9
		supply_limit = 6
		
		terrain_override = {
			94 77 120 102 69 61 134 139 362 29 282 278 42 40 323 322 222 223 
			
			#East Dameshead
			394 289
			
			#Borders
			418 321 414 918 318
			
			#Esmaria
			264 917 269 273 272 900 309 259
			
			#Escann
			795 853 814
			
			#Alenic Reach
			722 712 713 714 
		}
	}
	
	mountain = {
		color = { 105 24 4 }

		type = mountains
		sound_type = mountain

		movement_cost = 1.5
		defence = 2
		local_defensiveness = 0.25
		cs_only_local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.75
		supply_limit = 4
		
		terrain_override = {
			64 62 65 350 263 356 270 912 4 
			441 438 436 
		}
	}

	impassable_mountains = {
		color = { 128 128 128 }

		sound_type = desert

		movement_cost = 8.0
		defence = 6
		cs_only_local_development_cost = 10		
	}

	grasslands = {
		color = { 130 255 47 }

		type = plains
		sound_type = plains

		movement_cost = 1.0
		supply_limit = 8
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1
		
		terrain_override = {
			89 
		}
	}

	jungle = {
		color = { 98 163 18 }

		type = jungle
		sound_type = jungle

		movement_cost = 1.5
		defence = 1
		cs_only_local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.8
		supply_limit = 5
		
		terrain_override = {
			1057 1093 1092
		}
	}	
	
	marsh = {
		color = { 13 189 130 }

		type = marsh
		sound_type = forest

		movement_cost = 1.3
		defence = 1
		cs_only_local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85
		supply_limit = 5

		terrain_override = {
			#North Salahad
			463 466 503 464 462 467 546
		
			#Bulwar
			565 564 572 561 562 563
		
		}
	}
	
	desert = {
		color = { 242 242 111 }

		type = desert
		sound_type = desert

		movement_cost = 1.05
		supply_limit = 4
		cs_only_local_development_cost = 0.50
		nation_designer_cost_multiplier = 0.8
		
		terrain_override = { 
		}
	}
	
	coastal_desert = {
		color = { 255 211 110 }

		type = desert
		sound_type = desert

		movement_cost = 1.0
		cs_only_local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.9		
		supply_limit = 4

		terrain_override = {
			2124 
		}	
	}
	
	coastline = {
		color = { 49 175 191 }

		sound_type = sea

		movement_cost = 1.0
		cs_only_local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85		
		supply_limit = 6 
		
		terrain_override = {
			9 14 146 169 51 406 452 1086 1071 931 930 929 1079 1078 1077 1059 
		}
	}	
	
	drylands = {
		color = { 232 172 102 }		

		type = plains
		sound_type = plains

		movement_cost = 1.00
		cs_only_local_development_cost = 0.05
		nation_designer_cost_multiplier = 0.95
		supply_limit = 7
		allowed_num_of_buildings = 1

		terrain_override = {
			386 385 387 389 384 400 403 382 404
			
			#Bulwar
			534 537 539 540
			570 568 559 595 594 593 589 590 611 628 624 629 630 633 636 637 614 620 623
		}		
	}

	highlands = {
		color = { 176 129 21 }

		type = hills
		sound_type = mountain
		
		supply_limit = 6
		movement_cost = 1.40
		defence = 1
		local_defensiveness = 0.1
		cs_only_local_development_cost = 0.2
		nation_designer_cost_multiplier = 0.9	

		terrain_override = {
			179 180 182 184 196 197 200 371 370 317 316 375 376 291 290 374 379 35 369 380 36 399 381 405 393 391
			409 913
			
			#Escann
			
			
			#Bulwar
			529 520 522 518 580
			617 618 577 573 679 597 693 692 686 691
			551 552 555 554 658	545 654 663 664 665 619 671 670 672 673 674
		}
	}

	savannah = {
		color = { 248 199 23  }

		sound_type = plains

		supply_limit = 6
		movement_cost = 1.00
		cs_only_local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.95	
		
		terrain_override = {
			2093 1056 1221 1084
		}		
	}
	
	steppe = {
		color = { 147 200 83  }

		type = plains
		sound_type = plains

		movement_cost = 1.00
		cs_only_local_development_cost = 0.20
		nation_designer_cost_multiplier = 0.9	
		supply_limit = 6
		
		terrain_override = {
		}	
	}	
}
	
##################################################################
### Graphical terrain
###		type	=	refers to the terrain defined above, "terrain category"'s 
### 	color 	= 	index in bitmap color table (see terrain.bmp)
###

terrain = {
	grasslands			= { type = grasslands		color = { 	0	 } }
	hills				= { type = hills			color = { 	1	 } }
	desert_mountain		= { type = mountain			color = { 	2	 } }
	desert				= { type = desert			color = { 	3	 } }

	plains				= { type = grasslands		color = { 	4	 } }
	terrain_5			= { type = grasslands		color = { 	5	 } }
	mountain			= { type = mountain			color = { 	6	 } }
	desert_mountain_low	= { type = desert			color = { 	7	 } }

	terrain_8			= { type = hills			color = { 	8	 } }
	marsh				= { type = marsh			color = { 	9	 } }
	terrain_10			= { type = farmlands		color = { 	10	 } }
	terrain_11			= { type = farmlands		color = { 	11	 } }

	forest_12			= { type = forest			color = { 	12	 } }
	forest_13			= { type = forest			color = { 	13	 } }
	forest_14			= { type = forest			color = { 	14	 } }
	ocean				= { type = ocean			color = { 	15	 } }

	snow				= { type = mountain 		color = { 	16	 } } # (SPECIAL CASE) Used to identify permanent snow
	inland_ocean_17 	= { type = inland_ocean		color = {	17	 } }

	coastal_desert_18	= { type = coastal_desert	color = { 	19	 } }
	coastline			= { type = coastline		color = { 	35	 } }
	
	savannah			= { type = savannah 		color = {	20	 } }
	drylands			= { type = drylands			color = {	22	 } }
	highlands			= { type = highlands		color = {	23	 } }
	dry_highlands		= { type = highlands		color = {	24	 } }
	
	woods				= { type = woods			color = { 	255	 } }
	jungle				= { type = jungle			color = { 	254	 } }
	
	terrain_21			= { type = farmlands		color = { 	21	 } }	
}

##################################################################
### Tree terrain
###		terrain	=	refers to the terrain tag defined above
### 	color 	= 	index in bitmap color table (see tree.bmp)
###

tree = {
	forest				= { terrain = forest 			color = { 	3 4 6 7 19 20	} }
	woods				= { terrain = woods 			color = { 	2 5 8 18	} }
	jungle				= { terrain = jungle 			color = { 	13 14 15	} }
	palms				= { terrain = desert 			color = { 	12	} }
	savana				= { terrain = grasslands 		color = { 	27 28 29 30	} }
}