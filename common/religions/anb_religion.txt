# If you add religions, and use those tags, do not change them without changing everywhere they are used.

# Uses all 'modifiers' possible thats exported as a Modifier.

cannorian = {
	can_form_personal_unions = yes
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	center_of_religion = 840 #Trialmount

	cannorian_pantheon = {
		color = { 204 204 204 }
		icon = 8
		allowed_conversion = {
			regent_court
		}
		country = {
			tolerance_heathen = 2
			tolerance_heretic = 2
			tolerance_own = 1
		}
		country_as_secondary = {
			tolerance_heathen = 2
			tolerance_heretic = 1
		}
		
		personal_deity = yes
		
		on_convert = {
			change_religion = cannorian_pantheon
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { BHAKTI }
	}
	
	regent_court = {
		color = { 204 204 204 }
		icon = 8
		allowed_conversion = {
			corinite
		}
		country = {
			tolerance_heathen = 2
			tolerance_heretic = 1
		}
		country_as_secondary = {
			tolerance_heathen = 2
		}
		
		personal_deity = yes
		hre_religion = yes

		on_convert = {
			change_religion = regent_court
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { OLD_PANTHEON }
	}
	
	corinite = {
		color = { 230 65 55 }
		icon = 2
		allowed_conversion = {
			regent_court #Back to mainline
		}
		allowed_center_conversion = {
			regent_court
		}
		
		defender_of_faith = yes
		
		country = {
			leader_land_shock = 1
			land_morale = 0.1
		}
		country_as_secondary = {
			land_morale = 0.05
		}
		
		hre_heretic_religion = yes
		fervor = yes
		
		will_get_center = { #Refered to even for initial CoR Spawn
			any_owned_province = {
				can_have_center_of_reformation_trigger = {
					RELIGION = corinite
				}
			}
		}

		on_convert = {
			change_religion = corinite
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
			
			if = {
				limit = {
					has_country_flag = reformation_money
					NOT = {
						calc_true_if = {
							all_province = {
								religion = corinite
								is_reformation_center = yes
							}
							amount = 3
						}
					}
				}
				hidden_effect = {
					random_owned_province = {
						limit = {
							can_have_center_of_reformation_trigger = {
								RELIGION = corinite
							}
						}
						change_religion = corinite
						add_reform_center = corinite
					}
				}
				custom_tooltip = CENTER_OF_REFORMATION_POSSIBLE
			}
			if = {
				limit = {
					NOT = { has_country_flag = reformation_money }
					NOT = {
						calc_true_if = {
							all_province = {
								religion = corinite
								is_reformation_center = yes
							}
							amount = 3
						}
					}
				}
				add_years_of_income = 1.0
				hidden_effect = {
					random_owned_province = {
						limit = {
							can_have_center_of_reformation_trigger = {
								RELIGION = corinite
							}
						}
						change_religion = corinite
						add_reform_center = corinite
					}
				}
				custom_tooltip = CENTER_OF_REFORMATION_POSSIBLE
			}
		}
		
		heretic = { AGRADOS }
		
		date = 1510.1.1
	}
	
	ravelian = {
		color = { 204 204 0 }
		icon = 1
		allowed_conversion = {
			regent_court
			corinite
		}
		allowed_center_conversion = {
			regent_court
			corinite
		}
		country = {
			tolerance_own = 2
			tolerance_heretic = -2
		}
		
		country_as_secondary = {
			tolerance_own = 1
			tolerance_heretic = -1
		}
		
		will_get_center = { #Refered to even for initial CoR Spawn
			any_owned_province = {
				can_have_center_of_reformation_trigger = {
					RELIGION = ravelian
					development = 20
				}
			}
		}
		
		on_convert = {
			change_religion = ravelian
			add_prestige = -100
			
			
			if = {
				limit = {
					has_country_flag = reformation_money
					NOT = {
						calc_true_if = {
							all_province = {
								religion = ravelian
								is_reformation_center = yes
							}
							amount = 3
						}
					}
				}
				hidden_effect = {
					random_owned_province = {
						limit = {
							can_have_center_of_reformation_trigger = {
								RELIGION = ravelian
							}
							development = 20	#so its an intellectual thing!
						}
						change_religion = ravelian
						add_reform_center = ravelian
					}
				}
				custom_tooltip = CENTER_OF_REFORMATION_POSSIBLE
			}
			if = {
				limit = {
					NOT = { has_country_flag = reformation_money }
					NOT = {
						calc_true_if = {
							all_province = {
								religion = ravelian
								is_reformation_center = yes
							}
							amount = 3
						}
					}
				}
				add_years_of_income = 1.0
				hidden_effect = {
					random_owned_province = {
						limit = {
							can_have_center_of_reformation_trigger = {
								RELIGION = ravelian
							}
						}
						change_religion = ravelian
						add_reform_center = ravelian
					}
				}
				custom_tooltip = CENTER_OF_REFORMATION_POSSIBLE
			}
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}			
			set_country_flag = reformation_money
			every_known_country = {
				limit = {
					reverse_has_opinion_modifier = {
						who = ROOT
						modifier = opinion_demote_pope
					}
				}
				reverse_remove_opinion = {
					who = ROOT
					modifier = opinion_demote_pope
				}
			}
		}	
		
		
		heretic = { PURITAN }
		
		date = 1550.1.1
		
		papacy = {
			papal_tag = A85
			election_cost = 5
			seat_of_papacy = 118 # rome
			
			levy_church_tax = {
				cost = 50
				potential = {
					NOT = { tag = A85 }
				}
				allow = {
					NOT = { war_with = A85 }
					# NOT = { has_country_modifier = papal_sanction_for_church_taxes }
				}
				# effect = {
					# add_country_modifier = {
						# name = "papal_sanction_for_church_taxes"
						# duration = 7300
					# }
				# }
				# ai_will_do = {
					# factor = 1
				# }				
			}			
			bless_monarch = {
				cost = 50
				potential = {
					NOT = { tag = A85 }
				}
				allow = {
					NOT = { war_with = A85 }
					NOT = { prestige = 100 }
					NOT = { has_country_modifier = papal_blessing }
				}
				effect = {
					add_country_modifier = {
						name = "papal_blessing"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
				}
			}
			indulgence_for_sins = {
				cost = 50
				potential = {
					government = monarchy
					NOT = { tag = A85 }
				}
				allow = {
					NOT = { war_with = A85 }
					NOT = { legitimacy = 100 }
					NOT = { has_country_modifier = papal_indulgence }
				}
				effect = {
					add_country_modifier = {
						name = "papal_indulgence"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
				}
			}	
			local_saint = {
				cost = 100
				potential = {
					NOT = { tag = A85 }
				}
				allow = {
					NOT = { war_with = A85 }
					NOT = { stability = 3 }
				}
				effect = {
					add_stability = 1
				}
				ai_will_do = {
					factor = 1
					modifier = {
						factor = 2
						NOT = { stability = 0 }
					}
				}
			}
			forgiveness_for_usury = {
				cost = 50
				potential = {
					NOT = { tag = A85 }
				}
				allow = {
					NOT = { war_with = A85 }
					NOT = { has_country_modifier = usury_forgiven }
				}
				effect = {
					add_country_modifier = {
						name = "usury_forgiven"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
					modifier = {
						factor = 0
						NOT = { num_of_loans = 1 }
					}
				}
			}			
			proclaim_holy_war = {
				cost = 50
				potential = {
					NOT = { tag = A85 }
				}
				allow = {
					is_at_war = yes
					NOT = { war_with = A85 }
					NOT = { has_country_modifier = papal_sanction_for_holy_war }
				}
				effect = {
					add_country_modifier = {
						name = "papal_sanction_for_holy_war"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
					modifier = {
						factor = 0
						manpower_percentage = 0.5
					}
				}
			}
			send_papal_legate = {
				cost = 50
				potential = {
					NOT = { tag = A85 }
				}
				allow = {
					NOT = { war_with = A85 }
					NOT = { has_country_modifier = papal_legate }
				}
				effect = {
					add_country_modifier = {
						name = "papal_legate"
						duration = 7300
					}
				}
				ai_will_do = {
					factor = 1
				}
			}	
			sanction_commercial_monopoly = {
				cost = 50
				potential = {
					NOT = { tag = A85 }
				}
				allow = {
					NOT = { war_with = A85 }
					NOT = { mercantilism = 100 }
				}
				effect = {
					add_mercantilism = 1
				}
				ai_will_do = {
					factor = 1
					modifier = {
						factor = 0
						NOT = { has_idea_group = trade_ideas }
					}
				}
			}			
		}
	}

	harmonized_modifier = harmonized_dharmic
	
	crusade_name = CRUSADE
}

gnomish_philosophy = {

	flags_with_emblem_percentage = 0 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	center_of_religion = 169 #Nimscodd

	the_thought = {
		color = { 198 182 212 }
		icon = 9
		country = {
			tolerance_heretic = 2
			adm_tech_cost_modifier = -0.1
		}
		country_as_secondary = {
			adm_tech_cost_modifier = -0.1
			stability_cost_modifier = -0.1
		}
		heretic = { TAOIST }
		uses_harmony = yes
	}

	crusade_name = CRUSADE
}

kheteratan = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	center_of_religion = 475 #Kheterat

	khetist = {
		color = { 252 162 100 }
		icon = 24
		allowed_conversion = {
			#sikhism
		}
		country = {
			tolerance_heathen = 2
			tolerance_own = 1
		}
		country_as_secondary = {
			tolerance_heathen = 1
			tolerance_own = 1
		}
		
		province = {
			local_missionary_strength = -0.02	#its harder to convert them 
		}
		
		on_convert = {
			change_religion = khetist
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { ELIKHETIST }
	}
	
	mother_akan = {
		color = { 200 162 100 }
		icon = 19
		allowed_conversion = {
			#sikhism
		}
		country = {
			defensiveness = 0.1
			tolerance_own = 1
		}
		country_as_secondary = {
			defensiveness = 0.1
		}
		
		province = {
			local_missionary_strength = -0.02	#its harder to convert them 
		}
		
		on_convert = {
			change_religion = mother_akan
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { FATHER_AKAN }
	}
	
}

gnollish = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }

	xhazobkult = {
		color = { 181 230 29 }
		icon = 17
		allowed_conversion = {
			#sikhism
		}
		country = {
			raze_power_gain = 0.2
			land_morale = 0.1
		}
		country_as_secondary = {
			land_morale = 0.1
			raze_power_gain = 0.1
		}
		
		province = {
			local_missionary_strength = -0.02	#its harder to convert them 
		}
		
		on_convert = {
			change_religion = xhazobkult
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { BAINIST }
	}
	
}

dwarven = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }

	ancestor_worship = {
		color = { 99 99 99 }
		icon = 15
		allowed_conversion = {
			#sikhism
		}
		country = {
			same_culture_advisor_cost = -0.33
			defensiveness = 0.25
		}
		country_as_secondary = {
			same_culture_advisor_cost = -0.33
		}
		
		on_convert = {
			change_religion = ancestor_worship 
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { DEIST }
	}
	
}

elven = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }

	elven_forebears = {
		color = { 186 241 248 }
		icon = 16
		allowed_conversion = {
			#sikhism
		}
		country = {
			prestige_decay = -0.02
			tolerance_heathen = 2
		}
		country_as_secondary = {
			prestige_decay = -0.01 
			tolerance_heathen = 2
		}
		
		on_convert = {
			change_religion = elven_forebears
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { GUARDIAN }
	}
	
}

bulwari = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	center_of_religion = 601 #Bulwar or something

	bulwari_sun_cult = {
		color = { 255 201 14 }
		icon = 18
		allowed_conversion = {
			#sikhism
		}
		country = {
			global_unrest = -2
			tolerance_own = 2
		} 
		country_as_secondary = {
			global_unrest = -1
			tolerance_own = 1
		}
		
		province = {
			local_missionary_strength = -0.02	#its harder to convert them 
		}
		
		on_convert = {
			change_religion = bulwari_sun_cult
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { ECLIPSE }
	}
	
}

gerudian = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	center_of_religion = 974 #Draksbur

	skaldhyrric_faith = {
		color = { 255 201 201 }
		icon = 23
		allowed_conversion = {
			#sikhism
		}
		country = {
			stability_cost_modifier = -0.1
			tolerance_own = 2
		} 
		country_as_secondary = {
			stability_cost_modifier = -0.1
		}
		personal_deity = yes
		
		province = {
			local_missionary_strength = -0.02	#its harder to convert them 
		}
		
		on_convert = {
			change_religion = skaldhyrric_faith
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { AWAKER }
	}
	
}

orcish = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	center_of_religion = 677 #Jorkad Hall Outside

	great_dookan = {
		color = { 43 106 62 }
		icon = 17
		allowed_conversion = {
			#sikhism
		}
		country = {
			land_morale = 0.1
			prestige_from_land = 0.25
		}
		country_as_secondary = {
			land_morale = 0.1
			prestige_from_land = 0.1
		}
		
		on_convert = {
			change_religion = great_dookan
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { DENIER }
	}
	
}

dragon_cult = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	center_of_religion = 189 #Deep Kobilderd

	kobold_dragon_cult = {
		color = { 3 85 124 }
		icon = 21
		allowed_conversion = {
			#sikhism
		}
		country = {
			loot_amount = 0.5
			tolerance_heretic = 2
		}
		country_as_secondary = {
			loot_amount = 0.2
			tolerance_heretic = 2
		}
		
		on_convert = {
			change_religion = kobold_dragon_cult
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { DENIER }
	}
	
}

goblin = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	goblinic_shamanism = {	#The plural for goblin is goblinoid
		color = { 127 76 76 }
		icon = 12
		country = {
			tolerance_heathen = 2
			loot_amount = 0.10
		}
		country_as_secondary = {
			tolerance_own = 2
			loot_amount = 0.25
		}

		fetishist_cult = yes
		heretic = { CHAOS_WORSHIPPERS }
	}
}

harpy_cults = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	the_hunt = {	#The plural for goblin is goblinoid
		color = { 173 148 124 }
		icon = 13
		country = {
			tolerance_own = 1
			leader_land_shock = 1
		}
		country_as_secondary = {
			leader_land_shock = 1
		}

		heretic = { PREY }
	}
}

aelantiri = {

	can_form_personal_unions = no
	flags_with_emblem_percentage = 33 # default == 0
	flag_emblem_index_range = { 1 57 }
	
	weeping_mother = {
		color = { 255 120 128 }
		icon = 10
		allowed_conversion = {
			#sikhism
		}
		country = {
			global_unrest = -2
			tolerance_own = 1
		}
		country_as_secondary = {
			global_unrest = -2
		}
		
		province = {
			local_missionary_strength = 0.02	#its easy to convert them
		}
		
		on_convert = {
			change_religion = weeping_mother 
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { WEEPING_TREE }
	}
	
	death_cult_of_cheshosh = {
		color = { 66 66 66 }
		icon = 12
		allowed_conversion = {
			#sikhism
		}
		country = {
			land_morale = 0.1 
			shock_damage = 0.1
		}
		country_as_secondary = {
			land_morale = 0.1 
		}
		
		province = {
			local_missionary_strength = 0.02	#its easy to convert them
		}
		
		on_convert = {
			change_religion = death_cult_of_cheshosh 
			add_prestige = -100
			
			add_country_modifier = {
				name = "conversion_zeal"
				duration = 3650
			}
		}
		
		heretic = { RUINED_GODS }
	}
}