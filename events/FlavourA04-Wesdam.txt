
namespace = flavor_wesdam

#Laurens choosing between High Lorentish and West Damerian culture
country_event = {
	id = flavor_wesdam.1
	title = flavor_wesdam.1.t
	desc = flavor_wesdam.1.d
	picture = GOOD_WITH_MONARCH_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = A04
		
		has_ruler = "Laurens Silmuna"
		
		NOT = { is_year = 1500 }
	}
	
	mean_time_to_happen = {
		months = 3
	}
	
	#High Lorentish culture is the future
	option = {
		name = flavor_wesdam.1.a
		
		set_ruler_culture = high_lorentish
		
		add_prestige = 20
		
		every_country = {	# Rose Party members dislike Moon Party
			limit = {
				capital_scope = { region = lencenor_region }
				primary_culture = high_lorentish
			}
			add_opinion = { who = ROOT modifier = A04_laurens_rejected_high_lorentish }
		}
	}
	
	#Embrace the culture of my ancestors
	option = {
		name = flavor_wesdam.1.b
		
		set_ruler_culture = west_damerian
		
		add_stability = 1
		
		every_country = {	# Rose Party members dislike Moon Party
			limit = {
				capital_scope = { region = west_dameshead_region }
				primary_culture = west_damerian
			}
			add_opinion = { who = ROOT modifier = A04_laurens_rejected_west_damerian }
		}
		
	}

}