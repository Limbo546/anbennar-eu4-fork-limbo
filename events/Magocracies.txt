########################################
# Events for magocracy
#
# written by Johan Andersson
########################################

# Anbennar Changes
# added NOT = { has_reform = adventurer_reform } to magocracy.1, adventurer ver in Adventurers.txt

namespace = magocracy

country_event = {
	id =  magocracy.0
	title =  magocracy.0.t
	desc =  magocracy.0.d
	picture = WAR_OF_THE_ROSES_eventPicture
	
	#fire_only_once = yes
	is_triggered_only = yes
	
	hidden = yes
	
	trigger = {
		has_heir = yes
	}
	
	option = {
		name = "magocracy.0.a"
	
		if = {
			limit = { 
				heir_mil = 2 
				NOT = { heir_mil = 4 }
			}
			define_heir_to_general = {
				fire = 1
				shock = 7
				manuever = 1
				siege = 2
			}
		}
		else_if = {
			limit = { 
				heir_mil = 4 
				NOT = { heir_mil = 5 }
			}
			define_heir_to_general = {
				fire = 1
				shock = 8
				manuever = 1
				siege = 2
			}
		}
		else_if = {
			limit = { 
				heir_mil = 5 
				NOT = { heir_mil = 6 }
			}
			define_heir_to_general = {
				fire = 2
				shock = 9
				manuever = 2
				siege = 4
			}
		}
		else_if = {
			limit = { 
				heir_mil = 6 
			}
			define_heir_to_general = {
				fire = 2
				shock = 10
				manuever = 2
				siege = 4
			}
		}
	}
}

country_event = {
	id = magocracy.1
	title = theocracy.1.t
	desc = theocracy.1.desc
	picture = RELIGION_eventPicture

	is_triggered_only = yes

	trigger = {
		OR = {
			has_reform = magocracy_reform
			has_reform = magisterium_reform
		}
		NOT = { has_country_flag = in_magocracy_heir_selection }
		has_government_attribute = heir
	}

	immediate = {
		hidden_effect = {
			set_country_flag = in_magocracy_heir_selection
			
			#FOOOOKIN UPDATE dsaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
			clr_country_flag = mago_gifted_academic_flag
			clr_country_flag = mago_talented_sorcerer_flag
			clr_country_flag = mago_powerful_battlemage_flag
			clr_country_flag = mago_noble_scion_flag
			clr_country_flag = mago_prominent_estate_member_flag
			
			#Random Provs
			random_core_province = {
				save_event_target_as = gifted_academic_province
			}
			random_core_province = {
				save_event_target_as = charismatic_sorcerer_province
			}
			random_core_province = {
				save_event_target_as = powerful_battlemage_province
			}
			
			#Mages Estate
			if = {
				limit = {
					any_owned_province = {
						has_estate = estate_mages
					}
				}
				random_owned_province = {
					limit = { has_estate = estate_mages }
					save_event_target_as = mage_province
				}
			}
		}
	}

	option = {
		name = magocracy.1.a		# A Gifted Academic
		set_country_flag = mago_gifted_academic_flag # USED IN DEVOTION.TXT
		if = {
			limit = {
				has_saved_event_target = gifted_academic_province
			}
			define_heir = {
				age = 55
				hidden = yes
				culture = event_target:gifted_academic_province
				adm = 2
			}
		}
		else = {
			define_heir = {
				age = 55
				hidden = yes
				adm = 2
			}
		}
		add_adm_power = 20
		add_dip_power = 20
		add_mil_power = 20
		clr_country_flag = in_magocracy_heir_selection
		custom_tooltip = theocracy.1.tt
		
		hidden_effect = {
			random_list = {
				10 = {
					add_heir_personality = mage_personality
				}
				90 = {
				
				}
			}
		}
	}
	
	option = {
		name = magocracy.1.b		# A Charismatic Sorcerer
		set_country_flag = mago_talented_sorcerer_flag # USED IN DEVOTION.TXT
		if = {
			limit = {
				has_saved_event_target = charismatic_sorcerer_province
			}
			define_heir = {
				age = 55
				hidden = yes
				culture = event_target:charismatic_sorcerer_province
				dip = 2
			}
		}
		else = {
			define_heir = {
				age = 55
				hidden = yes
				dip = 2
			}
		}
		add_devotion = 10
		clr_country_flag = in_magocracy_heir_selection
		custom_tooltip = theocracy.1.tt
		
		hidden_effect = {
			random_list = {
				10 = {
					add_heir_personality = mage_personality
				}
				90 = {
				
				}
			}
		}
	}
	
	option = {
		name = magocracy.1.c		# A Powerful Battlemage
		set_country_flag = mago_powerful_battlemage_flag # USED IN DEVOTION.TXT
		if = {
			limit = {
				has_saved_event_target = powerful_battlemage_province
			}
			define_heir = {
				age = 55
				hidden = yes
				culture = event_target:powerful_battlemage_province
				mil = 2
			}
		}
		else = {
			define_heir = {
				age = 55
				hidden = yes
				mil = 2
			}
		}
		clr_country_flag = in_magocracy_heir_selection
		custom_tooltip = theocracy.1.tt
		
		custom_tooltip = tooltip_powerful_battlemage
		hidden_effect = { country_event = { id = magocracy.0 days = 28 } }
		
		hidden_effect = {
			random_list = {
				10 = {
					add_heir_personality = mage_personality
				}
				90 = {
				
				}
			}
		}
	}

	option = {
		name = magocracy.1.dd		# A Noble Scion
		trigger = {
			any_neighbor_country = {
				government = monarchy
				has_regency = no
				religion = ROOT
			}
		}
		if = {
			limit = {
				has_estate = estate_nobles
			}
			add_estate_loyalty = {
				estate = estate_nobles
				loyalty = 10
			}
		}
		if = {
			limit = {
				has_estate = estate_mages
			}
			add_estate_loyalty = {
				estate = estate_mages
				loyalty = -10
			}
		}	
		set_country_flag = mago_noble_scion_flag # USED IN DEVOTION.TXT
		random_neighbor_country = { 
			limit = {
				government = monarchy
				has_regency = no
				religion = ROOT
			}
			ROOT = { 
				define_heir = { 
					dynasty = PREV 
					age = 55
					culture = PREV
					hidden = yes
				} 
			}
			add_opinion = { who = ROOT modifier = opinion_magocracy_noble }
		}
		clr_country_flag = in_magocracy_heir_selection
		custom_tooltip = theocracy.1.tt
		
		hidden_effect = {
			random_list = {
				10 = {
					add_heir_personality = mage_personality
				}
				90 = {
				
				}
			}
		}
	}

	option = {
		name = magocracy.1.e		# A Prominent Member of the Mages Estate
		trigger = { has_estate = estate_mages }
		if = {
			limit = {
				has_estate = estate_mages
			}
			add_estate_loyalty = {
				estate = estate_mages
				loyalty = 15
			}
		}
		set_country_flag = mago_prominent_estate_member_flag # USED IN DEVOTION.TXT
		
		if = {
			limit = {
				has_saved_event_target = mage_province
			}
			define_heir = {
				age = 55
				hidden = yes
				culture = event_target:mage_province
			}
		}
		else = {
			define_heir = {
				age = 55
				hidden = yes
			}
		}
		add_years_of_income = 0.35
		add_devotion = -5
		clr_country_flag = in_magocracy_heir_selection
		custom_tooltip = theocracy.1.tt
		
		hidden_effect = {
			random_list = {
				10 = {
					add_heir_personality = mage_personality
				}
				90 = {
				
				}
			}
		}
	}
}