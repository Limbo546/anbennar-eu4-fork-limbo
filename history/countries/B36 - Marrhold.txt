government = monarchy
add_government_reform = feudalism_reform
government_rank = 2
primary_culture = marrodic
religion = regent_court
technology_group = tech_cannorian
capital = 896
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_centralized_flag }