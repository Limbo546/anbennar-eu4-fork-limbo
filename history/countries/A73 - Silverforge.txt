government = republic
add_government_reform = noble_elite_reform
government_rank = 1
primary_culture = silver_dwarf
religion = regent_court
technology_group = tech_dwarven
national_focus = DIP
capital = 308

elector = yes

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1422.1.1 = { set_country_flag = lilac_wars_moon_party }

1425.2.23 = {
	monarch = {
		name = "Foldan III"
		dynasty = "Silverhamer"
		birth_date = 1379.1.3
		adm = 4
		dip = 2
		mil = 2
	}
}