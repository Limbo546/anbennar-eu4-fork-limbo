# No previous file for Olavsborg
owner = Z13
controller = Z13
add_core = Z13
culture = olavish
religion = skaldhyrric_faith

hre = no

base_tax = 3
base_production = 2
base_manpower = 2

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind