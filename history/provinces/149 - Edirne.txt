#149 - Edirne

owner = A65
controller = A65
add_core = A65
culture = bluefoot_halfling
religion = regent_court
hre = no
base_tax = 3
base_production = 3
trade_goods = grain
base_manpower = 2
capital = "" 
is_city = yes



discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
