#403 - Nizwa

owner = F06
controller = F06
add_core = F06
culture = desha
religion = mother_akan

hre = no

base_tax = 4
base_production = 3
base_manpower = 4

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish