#255 - Kalisz

owner = A25
controller = A25
add_core = A25
culture = crownsman
religion = regent_court

hre = yes

base_tax = 5
base_production = 3
base_manpower = 2

trade_goods = cloth

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish