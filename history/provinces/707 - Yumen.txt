# No previous file for Yumen
owner = Z23
controller = Z23
add_core = Z23
culture = white_reachman
religion = regent_court

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish