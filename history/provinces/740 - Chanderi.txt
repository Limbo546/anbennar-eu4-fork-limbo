# No previous file for Chanderi
owner = Z18
controller = Z18
add_core = Z18
culture = gray_orc
religion = great_dookan
capital = ""


hre = no

base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = unknown
center_of_trade = 1

native_size = 43
native_ferocity = 8
native_hostileness = 8

add_permanent_province_modifier = {
	name = ruined_castanorian_citadel
	duration = -1
}