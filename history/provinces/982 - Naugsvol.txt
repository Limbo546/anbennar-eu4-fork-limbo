# No previous file for Naugsvol
owner = Z12
controller = Z12
add_core = Z12
culture = olavish
religion = skaldhyrric_faith

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = naval_supplies

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind